package cz.cvut.fel.ts1.shop;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

class OrderTest {
    Order order1, order2, order3,order4;
    ShoppingCart shoppingCart;
    @BeforeEach
    void SetUp(){
        shoppingCart = mock(ShoppingCart.class);
        order1 = new Order(shoppingCart,"jenda","technicka2");
        order2 = new Order(shoppingCart,"jenda","technicka2",1);
        order3 = new Order(shoppingCart,null,null,0);

    }
    @Test
    void testConstructor1(){
        assertNotNull(order1);
        assertEquals("jenda", order1.getCustomerName());
        assertEquals("technicka2", order1.getCustomerAddress());
    }
    @Test
    void testConstructor2(){
        assertNotNull(order2);
        assertEquals("jenda", order2.getCustomerName());
        assertEquals("technicka2", order2.getCustomerAddress());
        assertEquals(1, order2.getState());

    }
    @Test
    void testNull(){
        Assertions.assertNull(order4);
    }


}