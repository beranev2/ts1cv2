package cz.cvut.fel.ts1.archive;

import cz.cvut.fel.ts1.shop.Order;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

class PurchasesArchiveTest {
    //Order order = mock(Order.class);
    ArrayList<Order> orderArchive;
    ItemPurchaseArchiveEntry itemPurchaseArchiveEntry;
    PurchasesArchive purchasesArchive = new PurchasesArchive();
    @BeforeEach
    void setUp() {
        orderArchive = mock(ArrayList.class);
        itemPurchaseArchiveEntry = mock(ItemPurchaseArchiveEntry.class);
        purchasesArchive = new PurchasesArchive();
        PurchasesArchive purchasesArchive = new PurchasesArchive();
    }

    @Test
    void printItemPurchaseStatistics() {
    }

    @Test
    void getHowManyTimesHasBeenItemSold() {
        Assertions.assertEquals(1,purchasesArchive.getHowManyTimesHasBeenItemSold(itemPurchaseArchiveEntry.getRefItem()));
    }

    @Test
    void putOrderToPurchasesArchive() {
    }
}