package cz.cvut.fel.ts1.shop;

import cz.cvut.fel.ts1.archive.PurchasesArchive;
import cz.cvut.fel.ts1.shop.EShopController;
import cz.cvut.fel.ts1.storage.Storage;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.lang.reflect.Field;

import static org.mockito.Mockito.mock;

public class EShopControllerTest {
    private Storage mockStorage;

    private ShoppingCart mockShoppingCart;
    private PurchasesArchive mockArchive;
    private EShopController eShopController;

    @BeforeEach
    public void initData(){
        eShopController = new EShopController();
        mockStorage = new Storage();
        mockArchive = new PurchasesArchive();
        mockShoppingCart = new ShoppingCart();
        EShopController.startEShop();
    }


    @Test
    public void EShopControllerProcessTest(){
        StandardItem standardItem1 = new StandardItem(1,"a",50,"a",50);
        StandardItem standardItem2 = new StandardItem(2,"b",60,"b",60);
        mockStorage.insertItems(standardItem1,2);
        mockStorage.insertItems(standardItem2,20);
        Assertions.assertEquals(2,mockStorage.getItemCount(1));
        Assertions.assertEquals(20,mockStorage.getItemCount(2));
        mockShoppingCart.addItem(standardItem1);
        mockShoppingCart.addItem(standardItem1);
        mockShoppingCart.addItem(standardItem1);
        mockShoppingCart.addItem(standardItem1);
        mockShoppingCart.removeItem(1);

        try{
            EShopController.purchaseShoppingCart(mockShoppingCart,"jenda","t2");
        }catch(Exception e) {
            Assertions.assertEquals(e.getMessage(), "No item in storage");
        }




    }
}

