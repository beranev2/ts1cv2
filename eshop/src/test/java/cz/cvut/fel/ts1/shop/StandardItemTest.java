package cz.cvut.fel.ts1.shop;

import org.junit.Before;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

class StandardItemTest {
    StandardItem standardItem;
    StandardItem standardItemToCompare;
    @Test
    void testEquals() {
        Assertions.assertEquals(standardItem, standardItemToCompare);
    }

    @Test
    void copy() {
        assertEquals(standardItem, standardItem.copy());
    }

    @Test
    void constructorTest(){
        assertNotNull(standardItem);
        assertEquals(1,standardItem.getID());
        assertEquals("car",standardItem.getName());
        assertEquals(50,standardItem.getPrice());
        assertEquals("BMW",standardItem.getCategory());
        assertEquals(20,standardItem.getLoyaltyPoints());
    }

    @BeforeEach
    void setUp() {
        standardItem = new StandardItem(1,"car",50,"BMW",20);
        standardItemToCompare = mock(StandardItem.class);
    }
}