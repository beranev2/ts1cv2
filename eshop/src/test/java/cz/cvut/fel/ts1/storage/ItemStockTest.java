package cz.cvut.fel.ts1.storage;

import cz.cvut.fel.ts1.shop.Item;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.mock;

class ItemStockTest {
    private ItemStock itemStock;
    public Item item;

    @BeforeEach
    public void setItemStock(){
        item = mock(Item.class);
        itemStock = new ItemStock(item);
    }

    @Test
    void increaseItemCount() {
        itemStock.IncreaseItemCount(1);
        Assertions.assertEquals(1,itemStock.getCount());
    }

    @Test
    void decreaseItemCount() {
        itemStock.decreaseItemCount(1);
        Assertions.assertEquals(-1,itemStock.getCount());
    }
}