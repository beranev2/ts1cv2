package lab03;


import cz.cvut.fel.ts1.Calculator;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class CalculatorTest {
    Calculator calculator = new Calculator();
    //@ParametrizedTest(name = "Adds {0} tpo {1} to {2}");

    @Test
    public void assertAdd(){
        Assertions.assertEquals(4,calculator.add(1,3));
    }

    @Test
    public void assertSubtract(){
        Assertions.assertEquals(0,calculator.subtract(3,3));
    }

    @Test
    public void assertMultiply(){
        Assertions.assertEquals(3,calculator.multiply(1,3));
        Assertions.assertEquals(3,calculator.multiply(1,3));

    }

    @Test
    public void whenExceptionThrown_thenAssertionSucceeds() {
        Exception exception = assertThrows(NumberFormatException.class, () -> {
            Integer.parseInt("1a");
        });

        String expectedMessage = "For input string";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    public void assertDivide(){
        Assertions.assertEquals(1,calculator.divide(3,3));
        Assertions.assertEquals(0,calculator.divide(2,0));
        assertThrows(ArithmeticException.class,() -> calculator.divide(1,0),"Cant divide by 0");

    }



}
