package cz.cvut.fel.gameManager;

import cz.cvut.fel.maps.Map;
import cz.cvut.fel.utils.GameData;
import cz.cvut.fel.utils.GameState;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Assumptions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;
import java.text.AttributedCharacterIterator;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

class GameTest {
    Game game;
    GameData gameData;
    Player player;
    Map map;
    EndPoint endPoint;
    @BeforeEach
    void init(){
        game = new Game();
        gameData = new GameData();
        map = new Map();
        player = new Player(0,0,map);
        BufferedImage bufferedImage = mock(BufferedImage.class);
        endPoint = new EndPoint(5,0, bufferedImage);
    }

   }