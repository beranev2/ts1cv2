package cz.cvut.fel.gameManager;

import cz.cvut.fel.enemy.Goomba;
import cz.cvut.fel.friend.FireFlower;
import cz.cvut.fel.friend.Mushroom;
import cz.cvut.fel.friend.Prize;
import cz.cvut.fel.maps.Map;
import cz.cvut.fel.platforms.Platform;
import cz.cvut.fel.platforms.SurpriseBrick;
import cz.cvut.fel.utils.GameState;
import cz.cvut.fel.utils.PlayerState;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.text.DecimalFormat;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

class PlayerTest {
    Player player;
    Map map;
    @BeforeEach
    void init(){
        map = new Map();
        player = new Player(0,0,map);
        Mushroom mushroom = new Mushroom(0,0,mock(BufferedImage.class));
        map.getPrizeArrayList().add(mushroom);
        Goomba goomba = new Goomba(48,0,48,48,mock(BufferedImage.class));
        map.getGoombas().add(goomba);
        Prize prize = new Prize(0,0,10,10, mock(BufferedImage.class));
        SurpriseBrick surpriseBrick = new SurpriseBrick(0,0,10,10, mock(BufferedImage.class),prize);
        map.addSurpriseBrick(surpriseBrick);
    }
    @Test
    void touchCoin() {
        player.setCoins(99);
        player.touchCoin();
        Assertions.assertEquals(4,player.getLives());
    }
    @Test
    void updatePos() {
        player.setJump(true);
        player.updatePos();
        Assertions.assertEquals(6.8, player.getVelY(),0.01);
        player.setLeft(true);
        player.updatePos();
        Assertions.assertEquals(-2.0,player.getX(),0.01);
    }

    @Test
    void calculateMarioHorizontalMoveDistance() {
        BufferedImage bufferedImage = mock(BufferedImage.class);
        Platform platform = new Platform(0,0,10,10,bufferedImage);
        map.addPlatform(platform);
        Assertions.assertEquals(0, player.calculateMarioHorizontalMoveDistance(true));

    }

    @Test
    void hitSurpriseBrick() {
        map.surpriseBricks.getFirst().revealPrize();
        Assertions.assertTrue(map.surpriseBricks.getFirst().prize.isRevealed());
        Assertions.assertEquals(-48, map.surpriseBricks.getFirst().prize.getY());
    }
    void hitGoomba() {
        BufferedImage bufferedImage = mock(BufferedImage.class);
        Goomba goomba = new Goomba(0,0,10,10,bufferedImage);
        map.addGoomba(goomba);
        player.touchGoombas(player.getPlayerHitbox());
        Assertions.assertEquals(2,player.getLives());
    }

    @Test
    void updateFiring() {
        player.setPlayerState(PlayerState.FIRE);
        player.setShootFireball(true);
        player.updateFiring();
        Assertions.assertEquals(1,player.getFireballs().toArray().length );
        Assertions.assertTrue(player.isShooting());
    }


    @Test
    void hitFireflowerAndKillGoomba() {
        player.touchFireflower();
        Assertions.assertEquals(PlayerState.FIRE,player.getPlayerState());
        BufferedImage bufferedImage = mock(BufferedImage.class);
        Goomba goomba = new Goomba(90,0,10,10,bufferedImage);
        ArrayList<Goomba> goombaArrayList = new ArrayList<>();
        map.getGoombas().add(goomba);
        Assertions.assertNotNull(goomba);
        map.addGoomba(goomba);
        player.setShootFireball(true);
        Assertions.assertTrue(player.isShootFireball());
        player.updateFiring();
        Assertions.assertTrue(player.isShooting());
        Assertions.assertEquals(1,player.getFireballs().toArray().length);
        Assertions.assertEquals(0,goombaArrayList.toArray().length);
    }

    @Test
    void hitMushroomAndTouchGoomba() {
        player.hitPrize();
        Assertions.assertEquals(PlayerState.BIG,player.getPlayerState());
        player.setX(45);
        Assertions.assertEquals(45,player.getX());
        player.touchGoombas(player.getPlayerHitbox());
        Assertions.assertEquals(PlayerState.NORMAL,player.getPlayerState());
    }

    @Test
    void hitCoinsToGainLiveThenTouchGoomba() {
        player.setCoins(99);
        player.touchCoin();
        Assertions.assertEquals(4,player.getLives());
        player.setX(45);
        player.touchGoombas(player.getPlayerHitbox());
        Assertions.assertEquals(3,player.getLives());
    }

    @Test
    void hit3goombasAndDie(){
        Assertions.assertEquals(3,player.getLives());
        Goomba goomba2 = new Goomba(2*48,0,48,48,mock(BufferedImage.class));
        map.getGoombas().add(goomba2);
        Goomba goomba3 = new Goomba(3*48,0,48,48,mock(BufferedImage.class));
        map.getGoombas().add(goomba3);
        Assertions.assertEquals(0,player.getX());
        for (int i = 1; i < 6; i++) {
            player.setX(player.getX()+i*50);
            player.touchGoombas(player.getPlayerHitbox());
        }
        Assertions.assertEquals(1,player.getLives());
    }



}