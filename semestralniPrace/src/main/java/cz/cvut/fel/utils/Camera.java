package cz.cvut.fel.utils;

import cz.cvut.fel.enemy.Goomba;
import cz.cvut.fel.gameManager.Player;
import cz.cvut.fel.friend.Prize;
import cz.cvut.fel.maps.Map;
import cz.cvut.fel.platforms.Platform;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;

//import static cz.cvut.fel.newfolder.levels.LevelManager.*;
import static cz.cvut.fel.gameManager.Game.*;
import static cz.cvut.fel.utils.Constants.*;


public class Camera {
    Map map;
    Player player;
    // BACKGROUND
    private String backgroundImageName = "background.jpg";
    private BufferedImage backgroundImage = LoadSave.GetSpriteAtlas("background.png");
    // real size of the above image
    private int backgroundWidth = GAME_WIDTH;
    private int backgroundHeight = GAME_HEIGHT;

    // initial scroll of background
    private int scrollX = -1;
    private int scrollY = 0;

    // how fast to scroll
    private final int scrollStep = 2;

    private int heroX = 340;
    private final int noGoZone = 750;
    private int scrolled = 0;
    public Camera(Map map, Player player) {
        this.map = map;
        this.player = player;
    }
    public Camera(Map map, Player player, int scrollXcount){
        this.map = map;
        this.player = player;
        if(scrollXcount<=GAME_WIDTH && scrollXcount>0){
            this.map.scrollMap(scrollXcount);
        }
    }

    /**
     * scrolls the map so that the player doesnt leave the screen
     */
    public void doMove() {
        int xSteps = 1;
//        if is hero close to the zone, where he is supposed to move and the background is not scrollled completely, scroll the map
        if (isHeroNearToEdge(noGoZone)) {
            if (!isBgrScrolledToEdge()) {
                if(player.calculateMarioHorizontalMoveDistance(true)>0){
                    scrollX -= xSteps * scrollStep;
                    //if(player.setX(player.getX()+player.calculateMarioHorizontalMoveDistance(true));
                    map.scrollMap(scrollStep);
                    increaseScrolled();
                }
            } else {
                 // background scrolled, but player can still go to the edge
                if (!isHeroNearToEdge( 0)) {
                    player.setX(player.getX()+player.calculateMarioHorizontalMoveDistance(true));
                }
            }
              //hero is not close to the nogo zone, so he can move on
        } else {
            player.setX(player.getX()+player.calculateMarioHorizontalMoveDistance(true));
        }
    }

    public void render(Graphics g){
        g.drawImage(backgroundImage, this.getScrollX(), this.getScrollY(), this.getBackgroundWidth(), this.getBackgroundHeight(), null);

        for (Platform platform : map.getPlatformArrayList()) {
            g.drawImage(platform.bufferedImage, platform.getX(), platform.getY(), null);
        }
        for (Goomba goomba : map.getGoombas()) {
            g.drawImage(goomba.bufferedImage, goomba.getX(), goomba.getY(), null);
            goomba.drawHitbox(g);
        }

        for (Prize prize: map.getPrizeArrayList()){
            prize.draw(g);
        }
        g.drawImage(map.getEndPoint().getBufferedImage(),map.getEndPoint().getX(),map.getEndPoint().getY(),null);
        g.drawImage(map.getCoinCounterImage(),10,10,null);
        g.drawImage(map.getLiveCounterImage(),128,10,48, 48,null);
        g.setFont(new Font("Emulogic", Font.PLAIN,48));
        g.setColor(Color.BLACK);
        g.drawString(Integer.toString(player.getCoins()),64,56);
        g.drawString(Integer.toString(player.getLives()),176 ,56);
    }

    public String getBackgroundImageName() {
        return backgroundImageName;
    }
    public int getBackgroundWidth() {
        return backgroundWidth;
    }
    public int getBackgroundHeight() {
        return backgroundHeight;
    }
    public int getScrollX() {
        return scrollX;
    }
    public int getScrollY() {
        return scrollY;
    }
    private boolean isBgrScrolledToEdge() {
        return scrollX <= GAME_WINDOW_WIDTH - GAME_WIDTH;
    }
    private boolean isHeroNearToEdge(int distanceToEdge) {
        return heroX >= GAME_WINDOW_WIDTH - PLAYER_WIDTH - distanceToEdge;
    }
    public void setHeroX(int heroX) {
        this.heroX = heroX;
    }
    public int getScrolled() {
        return scrolled;
    }
    public void increaseScrolled() {
        this.scrolled++;
    }
}


