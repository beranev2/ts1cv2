package cz.cvut.fel.utils;

public class Constants {
    public static int PLAYER_SCALE = 3;
    public static int PLAYER_WIDTH = 24 * PLAYER_SCALE;
    public static int PLAYER_BIGGER_HEIGHT = 30 * PLAYER_SCALE;
    public static int PLAYER_SMALLER_HEIGHT = 15 * PLAYER_SCALE;
    public static int PLAYER_HITBOX_WIDTH = 10 * PLAYER_SCALE;

    public final static int TILES_IN_WIDTH = 200;
    public final static int TILES_IN_HIGHT = 15;
    private final static int TILES_DEFAULT_SIZE = 48;
    public final static float GAME_SCALE = 1.0f;
    public final static int TILES_SIZE = (int) (TILES_DEFAULT_SIZE* GAME_SCALE);
    public final static int GAME_HEIGHT = TILES_SIZE*TILES_IN_HIGHT;
    public final static int GAME_WIDTH = TILES_SIZE*TILES_IN_WIDTH;
    public final static int GAME_WINDOW_WIDTH = (int) (1500*GAME_SCALE);
    public final static int GROUND_LINE = TILES_SIZE*13;
}
