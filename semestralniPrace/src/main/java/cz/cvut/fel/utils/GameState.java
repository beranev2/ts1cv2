package cz.cvut.fel.utils;

public enum GameState {
    PLAYING, PAUSED, WON, LOST, HOWTO, ABOUT, NOSELECTION
}
