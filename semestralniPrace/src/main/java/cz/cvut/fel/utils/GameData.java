package cz.cvut.fel.utils;

import cz.cvut.fel.enemy.Goomba;
import cz.cvut.fel.friend.Prize;

import java.util.ArrayList;

/**
 * class used for storing data when saving or reading fromfile
 */
public class GameData {
    public int coins, lives, playerX, playerY,scrolled;

    public PlayerState playerState;

    public GameData(){}

    public GameData(int playerX, int playerY, int lives, int scrolled, int coins, PlayerState playerState) {
        this.lives = lives;
        this.playerY = playerY;
        this.playerX = playerX;
        this.scrolled = scrolled;
        this.coins = coins;
        this.playerState = playerState;

    }

    @Override
    public String toString() {
        return "GameData{" +
                "coins=" + coins +
                ", lives=" + lives +
                ", playerX=" + playerX +
                ", playerY=" + playerY +
                ", scrolled=" + scrolled +
                ", playerState=" + playerState +
                '}';
    }

    public PlayerState getPlayerState() {
        return playerState;
    }

    public void setPlayerState(PlayerState playerState) {
        this.playerState = playerState;
    }

    public int getCoins() {
        return coins;
    }

    public void setCoins(int coins) {
        this.coins = coins;
    }

    public int getLives() {
        return lives;
    }

    public void setLives(int lives) {
        this.lives = lives;
    }

    public int getPlayerX() {
        return playerX;
    }

    public void setPlayerX(int playerX) {
        this.playerX = playerX;
    }

    public int getPlayerY() {
        return playerY;
    }

    public void setPlayerY(int playerY) {
        this.playerY = playerY;
    }

    public int getScrolled() {
        return scrolled;
    }

    public void setScrolled(int scrolled) {
        this.scrolled = scrolled;
    }


}
