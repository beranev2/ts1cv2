package cz.cvut.fel.utils;

import cz.cvut.fel.gameManager.Player;
import cz.cvut.fel.utils.Camera;
import cz.cvut.fel.utils.GamePanel;
import cz.cvut.fel.utils.GameState;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import static java.awt.event.KeyEvent.*;
/**
 * class get user keyboard inputs
 */
public class KeyboardInputs implements KeyListener {
    public GamePanel gamePanel;
    private Player player;
    public Camera camera;
    public GameState gameState;
    public KeyboardInputs(GamePanel gamePanel, Camera camera, GameState gameState) {
        this.gamePanel = gamePanel;
        this.camera = camera;
        this.gameState = gameState;
    }
    @Override
    public void keyTyped(KeyEvent e) {
    }

    /**
     * manages the user input
     * @param e the event to be processed
     */
    @Override
    public void keyPressed(KeyEvent e) {
        //if(gameState == GameState.PLAYING) {
            switch (e.getKeyCode()) {
                case VK_ENTER:
                    gamePanel.getGame().setShowHowTo(false);
                    //gamePanel.getGame().setGameState(GameState.PLAYING);
                    break;
            }
            if(!gamePanel.getGame().isShowHowTo() && gamePanel.getGame().getGameState() != GameState.LOST ) {
                switch (e.getKeyCode()) {
                    case VK_LEFT,VK_A:
                        gamePanel.getGame().getPlayer().setLeft(true);
                        break;
                    case VK_RIGHT,VK_D:
                        gamePanel.getGame().getPlayer().setRight(true);
                        //camera.doMove();
                        break;
                    case VK_UP, VK_SPACE,VK_W:
                        gamePanel.getGame().getPlayer().setJump(true);
                        break;
                    case VK_X:
                        gamePanel.getGame().getPlayer().setShootFireball(true);
                        break;
                    case VK_S:
                        gamePanel.getGame().saveGame("savegame1.json");
                }
            }
    }
    @Override
    public void keyReleased(KeyEvent e) {
       // if(gameState == GameState.PLAYING) {
            switch (e.getKeyCode()) {
                case VK_LEFT:
                    gamePanel.getGame().getPlayer().setLeft(false);
                    break;
                case VK_DOWN:
                    gamePanel.getGame().getPlayer().setDown(false);
                    break;
                case VK_RIGHT:
                    gamePanel.getGame().getPlayer().setRight(false);

                    break;
                case VK_UP, VK_SPACE:
                    gamePanel.getGame().getPlayer().setJump(false);

                    break;
                case VK_X:
                    gamePanel.getGame().getPlayer().setShootFireball(false);
                    gamePanel.getGame().getPlayer().setShooting(false);
                    break;
                case VK_ENTER:
                    break;
            }
    }
}
