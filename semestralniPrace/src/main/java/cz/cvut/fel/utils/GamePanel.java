package cz.cvut.fel.utils;

import cz.cvut.fel.gameManager.Game;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;

import static cz.cvut.fel.gameManager.Game.*;
import static cz.cvut.fel.utils.Constants.GAME_HEIGHT;
import static cz.cvut.fel.utils.Constants.GAME_WINDOW_WIDTH;

/**
 * setting up gamepanel, which is basic component for swing
 */
public class GamePanel extends JPanel {
    private Game game;
    private int dx = 100, dy = 100;
    long lastCheck = 0;
    private BufferedImage Mario;
    private BufferedImage background;
    private BufferedImage normalWalkingMario;
    public GamePanel(Game game) {
        this.game = game;
        setPanelSize();
        addKeyListener(new KeyboardInputs(this,game.camera,game.getGameState()));
        //importImg();
    }
    private void setPanelSize(){
        Dimension dimension = new Dimension(GAME_WINDOW_WIDTH, GAME_HEIGHT);
        setMinimumSize(dimension);
        setPreferredSize(dimension);
        setMaximumSize(dimension);
    }
    private GameWindow gameWindow;


    /**
     *this is called through repaint() in the game class
     * used for rendering the game
     * @param g the <code>Graphics</code> object to protect
     */
    public void paintComponent(Graphics g){
        super.paintComponent(g);
        game.render(g);
    }
    public Game getGame() {
        return game;
    }
}


