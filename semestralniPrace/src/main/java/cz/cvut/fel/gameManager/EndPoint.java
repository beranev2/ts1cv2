package cz.cvut.fel.gameManager;

import java.awt.image.BufferedImage;

public class EndPoint extends GameObject{
    BufferedImage bufferedImage;
    public EndPoint(int x, int y, BufferedImage bufferedImage) {
        super(x, y, 48, 48,bufferedImage);
        this.x = x;
        this.y = y;
        this.bufferedImage = bufferedImage;
    }
}
