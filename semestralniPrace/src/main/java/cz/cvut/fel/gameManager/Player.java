package cz.cvut.fel.gameManager;

//import cz.cvut.fel.newfolder.levels.LevelManager;
import cz.cvut.fel.enemy.Goomba;
import cz.cvut.fel.friend.*;
import cz.cvut.fel.maps.Map;
import cz.cvut.fel.platforms.Platform;
import cz.cvut.fel.platforms.SurpriseBrick;
import cz.cvut.fel.utils.GameData;
import cz.cvut.fel.utils.LoadSave;
import cz.cvut.fel.utils.PlayerState;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

import static cz.cvut.fel.gameManager.Game.*;
import static cz.cvut.fel.utils.Constants.*;
import static cz.cvut.fel.utils.PlayerState.BIG;
import static cz.cvut.fel.utils.PlayerState.FIRE;

/**
 * constructor of player class
 */
public class Player {
    private ArrayList<Fireball> fireballs = new ArrayList<>();
    public BufferedImage fireballImage;
    private PlayerState playerState = PlayerState.NORMAL;
    public Map map;
    private int x, y;
    private int lives = 3;
    private int coins = 0;
    private float velY = 0;
    private float gravityAcc = 0.2f;
    private boolean isJumping;
    private boolean isFalling;
    public int currentPlayerHeight = PLAYER_SMALLER_HEIGHT;
    public int currentHitboxHeight = currentPlayerHeight;
    private boolean up = false, down = false, right = false, left = false, moving = false, jump = false;
    private boolean shootFireball = false;
    //gravity
    public float playerSpeed = 2.0f;
    private boolean isOnPlatform = false;

    /**
     * constructor of player class
     */
    public Player(int x, int y, Map map) {
        this.x = x;
        this.y = y;
        this.map = map;
        //this.collisions = new Collisions();
    }
    public void setMap(Map map) {
        this.map = map;
    }
    public Rectangle getPlayerHitbox() {
        return new Rectangle(x+7,y, PLAYER_HITBOX_WIDTH,currentPlayerHeight);
    }
    public void drawHitbox(Graphics graphics) {
        graphics.setColor(Color.BLACK);
        graphics.drawRect(getPlayerHitbox().x, getPlayerHitbox().y, getPlayerHitbox().width, getPlayerHitbox().height);

    }
    /**
     * hitbox is for collision detection, works as a simplification of the hero
     */
    public void updateHitbox() {
        getPlayerHitbox().height = currentHitboxHeight;
        getPlayerHitbox().x = x + 7;
        getPlayerHitbox().y = y;
    }

    public void render(Graphics g) {
        BufferedImage img = LoadSave.GetSpriteAtlas("hero.png");
        BufferedImage playerImage = null;
        if(playerState == PlayerState.NORMAL){
            playerImage = img.getSubimage(88, 7, PLAYER_WIDTH / PLAYER_SCALE, currentPlayerHeight / PLAYER_SCALE);
            currentPlayerHeight = PLAYER_SMALLER_HEIGHT;
            g.drawImage(playerImage, x, y, PLAYER_WIDTH, currentPlayerHeight, null);
        }
        if(playerState == BIG){
            playerImage = img.getSubimage(88, 80, PLAYER_WIDTH / PLAYER_SCALE, PLAYER_BIGGER_HEIGHT / PLAYER_SCALE);
            currentPlayerHeight = PLAYER_BIGGER_HEIGHT;
            g.drawImage(playerImage, x, y, PLAYER_WIDTH, PLAYER_BIGGER_HEIGHT, null);
        }
        if(playerState == FIRE){
            playerImage = img.getSubimage(88, 160, PLAYER_WIDTH / PLAYER_SCALE, PLAYER_BIGGER_HEIGHT / PLAYER_SCALE);
            currentPlayerHeight = PLAYER_BIGGER_HEIGHT;
            g.drawImage(playerImage, x, y, PLAYER_WIDTH, PLAYER_BIGGER_HEIGHT, null);
        }
        BufferedImage fireballImageAtlas = LoadSave.GetSpriteAtlas("sprite.png");
        fireballImage = fireballImageAtlas.getSubimage(3*48, 2*48, 48, 48);
        for(Fireball fireball:fireballs){
            g.drawImage(fireball.getImage(),fireball.getX(), fireball.getY(),fireball.getHeight(), fireball.getWidth(),null);
        }
    }
    private void touchMushroom() {
        this.setPlayerState(BIG);
    }
    public void touchFireflower() {
        this.setPlayerState(FIRE);
    }
    public void touchCoin() {
        this.increaseCoins();
        if (this.getCoins() == 100) {
            lives++;
        }
    }

    public void increaseCoins() {
        this.coins++;
    }
    public PlayerState getPlayerState() {
        return playerState;
    }

    /**
     * changes position according to user input, also manages falling, when not standing on the platfrom
     */
    void updatePos() {
        updateFiring();
        if (jump && !isJumping && !isFalling) {
            setJumping(true);
            setVelY(7);
        }
        jump();
        if (left) {
            this.setX(this.getX() - calculateMarioHorizontalMoveDistance(false));
            moving = true;
        }
        touchGoombas(this.getPlayerHitbox());
        //hitSurpriseBrick();
        hitPrize();
        if (!isOnGround() && !isJumping && !isFalling && !this.isOnPlatform && this.getVelY() == 0) {
            LOGGER.log(System.Logger.Level.INFO,"ioG = " + isOnGround() + " iJ = " + isJumping() + " ioP = " +
                    isOnPlatform() + " iF = " + isFalling() + " velY = " + this.getVelY() );
            this.setOnPlatform(false);
            this.setFalling(true);
        }
        this.calculateMarioVerticalMoveDistance(y);
    }
    /**
     * manages jumping and falling
     */
    private void jump() {
        if (isJumping && velY < 0) {
            isJumping = false;
            isFalling = true;
            System.out.println("switching to falling");
        } else if (isJumping) {
            velY = velY - gravityAcc;
            y = calculateMarioVerticalMoveDistance((int) (y - velY));
        }
        if (isFalling) {
            velY = velY + gravityAcc;
            y = calculateMarioVerticalMoveDistance((int) (y + velY));
        }
        if (y >= (GROUND_LINE - currentPlayerHeight)) {
            y = GROUND_LINE - currentPlayerHeight;
            getPlayerHitbox().y = GROUND_LINE - currentPlayerHeight;
            jump = false;
            isJumping = false;
            isFalling = false;
        }
    }
    /**
     *this method precalculates the position after the movement is done in the horizontal direction
     * checks for collisions with platforms and goombas
     * @param right sets the direction right (true) or left (false)
     * @return 0 if it is not possible to move there, otherwise returns playerspeed
     */
    public int calculateMarioHorizontalMoveDistance(boolean right) {
        int newX;
        if (right) {
            newX = this.getPlayerHitbox().x + (int) playerSpeed;
        } else {
            newX = this.getPlayerHitbox().x - (int) playerSpeed;
        }
        Rectangle newPosition = new Rectangle(newX, this.getPlayerHitbox().y, this.getPlayerHitbox().width, this.getPlayerHitbox().height);
        for (Rectangle inGameHitBox : map.getPlatformArrayListHitboxes()) {
            if (newPosition.intersects(inGameHitBox)
                    && (newPosition.x + newPosition.width > inGameHitBox.x
                    && isYCoordinateInRectangle(newPosition.y, inGameHitBox))) {
                return 0;
            }
        }
        touchGoombas(newPosition);
        return (int) playerSpeed;
    }

    /**
     * his method precalculates the position after the movement is done in the vertical direction
     * @param positionToBe
     * @return velocity
     */
    private int calculateMarioVerticalMoveDistance(int positionToBe) {
        int newY;
        newY = positionToBe;
        Rectangle newPosition = new Rectangle(this.getPlayerHitbox().x, newY, this.getPlayerHitbox().width, this.getPlayerHitbox().height);
        this.setOnPlatform(false);

        for (Platform inGamePlatform : map.getPlatformArrayList()) {
            if (newPosition.intersects(inGamePlatform.getHitbox())
                    && (newPosition.x + newPosition.width > inGamePlatform.getHitbox().x)) {

                //landing
                if (!isYCoordinateInRectangle(newPosition.y, inGamePlatform.getHitbox())) {
                    if (isFalling) {
                        this.y = inGamePlatform.getHitbox().y - currentPlayerHeight;
                        this.setOnPlatform(true);
                        this.setFalling(false);
                        this.setVelY(0);
                        System.out.println("im on platform");
                    }
                    return this.y;
                    //hitting from below or side
                } else {
                    if(inGamePlatform instanceof SurpriseBrick ){
                        ((SurpriseBrick) inGamePlatform).revealPrize();
                    }
                    this.setFalling(true);
                    this.setJumping(false);
                    this.setVelY(0);
                    System.out.println("hitting from below");
                    return inGamePlatform.getHitbox().y + inGamePlatform.getHitbox().height;
                }
            }

        }

        return positionToBe;
    }
    /**
     * checks if the upper left corner is within the given rectangle
     * @param y stands for y position of player
     * @param rectangle is mainly used for platforms
     * @return
     */
    public boolean isYCoordinateInRectangle(int y, Rectangle rectangle) {
        return y >= rectangle.y && y <= rectangle.y + rectangle.getHeight();
    }
    /**
     * iterates through array of goombas to see, if it hits some
     * @param rectangle
     */
    public void touchGoombas(Rectangle rectangle) {
        for (Goomba goomba : map.getGoombas()) {
            //System.out.println(rectangle.intersects(goomba.getHitbox()));
            if (rectangle.intersects(goomba.getHitbox()) && !goomba.isTouched()
            ) {
                System.out.println("hit goomba");
                this.onTouchGoomba(goomba);

            }
        }
    }

    /**
     * implements behavior on touch of enemy (goomba) according to player state
     * @param goomba
     */
    private void onTouchGoomba(Goomba goomba) {
        if (this.getPlayerState() == FIRE || this.getPlayerState() == BIG) {
            this.setPlayerState(PlayerState.NORMAL);
        } else {
            this.decreaseLives();
        }
        goomba.setTouched(true);
        goomba.hide();
    }
    /**implements the behavior when touching the prize according to type of prize
     *
     */
    public void hitPrize() {
        for (Prize prize : map.getPrizeArrayList()) {
            if (this.getPlayerHitbox().intersects(prize.getHitbox())) {
                if (prize instanceof Coin) {
                    if(!prize.isEmpty()){
                        touchCoin();
                    }
                    prize.hide();
                }
                if (prize instanceof FireFlower) {
                    if(!prize.isEmpty()){
                        touchFireflower();
                    }
                    prize.hide();
                }
                if (prize instanceof Mushroom) {
                    if(!prize.isEmpty()){
                        touchMushroom();
                    }
                    prize.hide();
                }
            }
        }
    }

    /**
     *
     * @return true if player stands on the ground line
     */
    private boolean isOnGround() {
        return this.y >= GROUND_LINE - currentPlayerHeight;
    }

    /**
     * updates location of fireballs and implements killing them
     */
    public void updateFiring() {
        if (playerState == FIRE) {
            if (shootFireball && !isShooting) {
                this.addFireball();
                this.setShooting(true);
            }
            updateFireballs();
            for(Fireball fireball:fireballs){
                if(!fireball.isEmpty()){
                    if (fireball.killGoomba(map.getGoombas())){
                        fireball.setEmpty(true);
                        fireball.hide();
                    }
                }
            }
        }
    }

    /**
     * updates location of single fireball
     */
    public void updateFireballs() {
        for (Fireball fireball : this.fireballs) {
            fireball.updateFireball();
        }
    }

    /**
     * inserts data from json file
     * @param dataFromfile
     */
    public void insertDataFromFile(GameData dataFromfile){
        this.setCoins(dataFromfile.getCoins());
        this.setLives(dataFromfile.getLives());
        this.setX(dataFromfile.getPlayerX());
        this.setY(dataFromfile.getPlayerY());
        this.setPlayerState(dataFromfile.getPlayerState());
    }
    public void addFireball() {
        Fireball fireball = new Fireball(this.x, this.y+50, fireballImage);
        fireballs.add(fireball);
    }
    public int getLives() {
        return lives;
    }
    public void decreaseLives() {
        this.lives--;
    }
    public boolean isLeft() {
        return left;
    }
    public void setLeft(boolean left) {
        this.left = left;
    }
    public boolean isUp() {
        return up;
    }
    public void setUp(boolean up) {
        this.up = up;
    }
    public boolean isRight() {
        return right;
    }
    public void setRight(boolean right) {
        this.right = right;
    }
    public boolean isDown() {
        return down;
    }
    public void setDown(boolean down) {
        this.down = down;
    }
    public float getVelY() {
        return velY;
    }
    public void setVelY(float velY) {
        this.velY = velY;
    }
    public boolean isJumping() {
        return isJumping;
    }
    public void setJumping(boolean jumping) {
        this.isJumping = jumping;
    }
    public boolean isFalling() {
        return isFalling;
    }
    public void setFalling(boolean falling) {
        this.isFalling = falling;
    }
    public boolean isOnPlatform() {
        return isOnPlatform;
    }
    public void setOnPlatform(boolean onPlatform) {
        isOnPlatform = onPlatform;
    }
    public void setJump(boolean jump) {
        this.jump = jump;
        //inAir = true;
    }
    public int getY() {
        return y;
    }
    public void setY(int y) {
        this.y = y;
    }
    public int getCoins() {
        return coins;
    }
    public void setCoins(int coins) {
        this.coins = coins;
    }
    public void setLives(int lives) {
        this.lives = lives;
    }
    public ArrayList<Fireball> getFireballs() {
        return fireballs;
    }
    public void setFireballs(ArrayList<Fireball> fireballs) {
        this.fireballs = fireballs;
    }
    public void setPlayerState(PlayerState playerState) {
        this.playerState = playerState;
    }
    public boolean isShootFireball() {
        return shootFireball;
    }
    public void setShootFireball(boolean shootFireball) {
        this.shootFireball = shootFireball;
    }
    private boolean isShooting = false;
    public boolean isShooting() {
        return isShooting;
    }
    public void setShooting(boolean shooting) {
        isShooting = shooting;
    }
    public int getX() {
        return x;
    }
    public void setX(int x) {
        this.x = x;
    }


}
