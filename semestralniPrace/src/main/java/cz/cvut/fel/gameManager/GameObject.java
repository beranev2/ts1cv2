package cz.cvut.fel.gameManager;

import java.awt.*;
import java.awt.image.BufferedImage;
/**
        this is the basic method, from which all prizes and enemies inherit
     */
public class GameObject {
    int x,y,height,width;
    BufferedImage bufferedImage;
    public GameObject(int x, int y, int width, int height, BufferedImage bufferedImage) {
        this.x = x;
        this.y = y;
        this.height = height;
        this.width = width;
        this.bufferedImage = bufferedImage;
    }
    public int getX() {
        return x;
    }
    public void setX(int x) {
        this.x = x;
    }
    public int getY() {
        return y;
    }
    public void setY(int y) {
        this.y = y;
    }
    public Rectangle getHitbox() {
        return new Rectangle(x,y,width,height);
    }
    public int getHeight() {
        return height;
    }
    public void setHeight(int height) {
        this.height = height;
    }
    public int getWidth() {
        return width;
    }
    public void setWidth(int width) {
        this.width = width;
    }
    public BufferedImage getBufferedImage() {
        return bufferedImage;
    }
    public void setBufferedImage(BufferedImage bufferedImage) {
        this.bufferedImage = bufferedImage;
    }
    @Override
    public String toString() {
        return "GameObject{" +
                "x=" + x +
                ", y=" + y +
                ", height=" + height +
                ", width=" + width +
                ", bufferedImage=" + bufferedImage +
                '}';
    }
}
