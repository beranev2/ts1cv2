package cz.cvut.fel.gameManager;
/**
 * @author Eva Beranova
 */

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import cz.cvut.fel.maps.Map;
import cz.cvut.fel.maps.MapBuilder;
import cz.cvut.fel.utils.*;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import static cz.cvut.fel.utils.Constants.GROUND_LINE;
import static cz.cvut.fel.utils.Constants.PLAYER_SMALLER_HEIGHT;

//import static jdk.jfr.internal.consumer.EventLog.update;


public class Game{

    private GameWindow gameWindow;
    private GamePanel gamePanel;
    //private Thread gameThread;
    private Player player;
    private BufferedImage howTo,lost,won;
    private final int UPS_SET = 200;
    private final static String SAVE_FILE_NAME = "savegame.json";

    public Map map;
    public Camera camera;
    public final static System.Logger LOGGER = System.getLogger(Game.class.getName());
    public GameState gameState;
    private GameData dataFromFile;
    private boolean showHowTo = true;
    private boolean saved = false;

    /**
     * constructor of game class
     */
    public Game() {
        this.setDataFromFile(this.readFromFile("savegame.json"));
        initClasees();
        this.gamePanel = new GamePanel(this);
        this.gameWindow = new GameWindow(gamePanel);
        gamePanel.requestFocus();
        startGameLoop();
    }
    /**
     * this method updates the game, namely player position and camera location
     * also checks for the game state
     */
    public void update() {
        //levelManager.update();
        camera.setHeroX(player.getX());
        player.setMap(map);
        if(player.isRight()){
            camera.doMove();
        }
        player.updatePos();
        player.updateHitbox();
        gameState = updateGamestate(player);
    }
    /**
     * constructor of player and map
     * if there is data from file, it inserts it into player and camera
     */

    private void initClasees(){
        player = new Player(200, GROUND_LINE-PLAYER_SMALLER_HEIGHT,map);

        LoadSave LoadSave = new LoadSave();
        MapBuilder mapBuilder = new MapBuilder(LoadSave);

        map = mapBuilder.createMap("Map 1.png");

       if(this.getDataFromFile() == null){
            LOGGER.log(System.Logger.Level.INFO, "gameData not found ");
            //System.out.println(this.getDataFromFile().toString());
            camera = new Camera(map,player);
            System.out.println(map.toString());
        }
        else{
            LOGGER.log(System.Logger.Level.INFO, "inserting gamedata from file ");
            camera = new Camera(map,player,this.getDataFromFile().scrolled);
            player.insertDataFromFile(this.getDataFromFile());
        }
        this.setGameState(GameState.PLAYING);
    }

    /**
     * starting a game loop from a thread
     */
    private void startGameLoop(){
        run();
    }
    /**
     * rendering graphics for level and the player, also renders graphics in case player loses or wins
     */
    public void render(Graphics g) {
        //g.drawImage(backgroundImage, Camera.getScrollX(), Camera.getScrollY(), Camera.getBackgroundWidth(), Camera.getBackgroundHeight(), null);
        camera.render(g);
        //map.renderMap(g);
        player.render(g);
        player.drawHitbox(g);
        if (showHowTo){
            g.drawImage(this.renderHowTo(),10,20,550,550,null);
        }
        if(gameState == GameState.LOST){
            LOGGER.log(System.Logger.Level.INFO, "LOST ");
            this.renderLost(g);

        }
        if(gameState == GameState.WON){
            LOGGER.log(System.Logger.Level.INFO, "WON ");

            this.renderWon(g);
        }
    }
    public BufferedImage renderHowTo(){
        this.setHowTo(LoadSave.GetSpriteAtlas("howTo4.png"));
        return this.getHowTo();
    }
    public BufferedImage renderWon(Graphics g){
        this.setWon(LoadSave.GetSpriteAtlas("won.png"));
        g.drawImage(this.getWon(),500,10,550,550,null);
        return this.getWon();
    }
    public BufferedImage renderLost(Graphics g){
        this.setLost(LoadSave.GetSpriteAtlas("lost.png"));
        g.drawImage(this.getLost(),500,10,550,550,null);
        return this.getLost();
    }

    /**
     *game loop with a timer
     */
//    @Override
    public void run() {
        double timePerUpdate = 1000000000.0 / UPS_SET;
        long previousTime = System.nanoTime();
        double deltaU = 0;
        while (true) {
            long currentTime = System.nanoTime();
            deltaU += (currentTime - previousTime) / timePerUpdate;
            previousTime = currentTime;
            if (deltaU >= 1) {
                update();
                gamePanel.repaint();
                deltaU--;
            }
        }
    }

    public GameState updateGamestate(Player player){
        if(player.getLives() <= 0){
            LOGGER.log(System.Logger.Level.INFO, "0 lives");
            return GameState.LOST;
        }
        if(player.getPlayerHitbox().intersects(map.getEndPoint().getHitbox())){
            LOGGER.log(System.Logger.Level.INFO, "won");
            return GameState.WON;
        }
        else{
            return this.getGameState();
        }
    }
    public void saveGame(String fileName) {
        if(!isSaved()){
            LOGGER.log(System.Logger.Level.INFO, "Saving game ");

            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
            GameData gameData = new GameData(player.getX(), player.getY(), player.getLives(), camera.getScrolled(),player.getCoins(), player.getPlayerState());
            try {
                objectMapper.writeValue(new File(fileName), gameData);
                this.setSaved(true);
            } catch (IOException e) {
                LOGGER.log(System.Logger.Level.ERROR, "Can't save game: " + e.getMessage());
            }
        }
    }

    public GameData readFromFile(String fileName){
        LOGGER.log(System.Logger.Level.INFO, "Reading from file ");
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
        try {
            InputStream is= GameData.class.getClassLoader().getResourceAsStream(fileName);
            if(is == null){
                LOGGER.log(System.Logger.Level.INFO, "IS is null ");
                return null;
            }
            GameData gameData1 = objectMapper.readValue(is, GameData.class);
            LOGGER.log(System.Logger.Level.INFO, "RETURNING GAME DATA ");
            return gameData1;

        } catch (IOException e) {
            LOGGER.log(System.Logger.Level.ERROR, "Can't read file: " + e.getMessage());
        }
        return null;
    }

    public GameState getGameState() {
        return gameState;
    }
    public void setGameState(GameState gameState) {
        this.gameState = gameState;
    }
    public GameData getDataFromFile() {
        return dataFromFile;
    }
    public void setDataFromFile(GameData dataFromFile) {
        this.dataFromFile = dataFromFile;
    }
    public BufferedImage getHowTo() {
        return howTo;
    }
    public void setHowTo(BufferedImage howTo) {
        this.howTo = howTo;
    }
    public Player getPlayer() {
        return player;
    }
    public boolean isShowHowTo() {
        return showHowTo;
    }
    public void setShowHowTo(boolean showHowTo) {
        this.showHowTo = showHowTo;
    }
    public BufferedImage getLost() {
        return lost;
    }
    public void setLost(BufferedImage lost) {
        this.lost = lost;
    }
    public BufferedImage getWon() {
        return won;
    }
    public void setWon(BufferedImage won) {
        this.won = won;
    }
    public boolean isSaved() {
        return saved;
    }
    public void setSaved(boolean saved) {
        this.saved = saved;
    }
}
