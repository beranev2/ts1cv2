package cz.cvut.fel.gameManager;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
public class Main {
    public static void main(String[] args) {
        Game game;
        if (args.length > 0 && Objects.equals(args[0], "logger-off")) {
            Logger.getLogger("").setLevel(Level.OFF);
        }
        game = new Game();
    }
}
