package cz.cvut.fel.enemy;

import cz.cvut.fel.gameManager.GameObject;

import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * this is an enemy class. On touch of this the player loses a life. It can only kill it with fireballs
 */
public class Goomba extends GameObject {
    private boolean touched = false;
    public Rectangle getHitbox() {
        return new Rectangle(this.getX(),this.getY(),this.getWidth(),this.getHeight());
    }
    public boolean isTouched() {
        return touched;
    }
    public void setTouched(boolean touched) {
        this.touched = touched;
    }
    public BufferedImage bufferedImage;
    public Goomba(int x, int y, int width, int height,BufferedImage img) {
        super(x,y,width,height,img);
        this.bufferedImage = img;
    }
    public void drawHitbox(Graphics graphics) {
        graphics.setColor(Color.pink);
        graphics.drawRect(this.getX(),this.getY(),this.getWidth(),this.getHeight());

    }
    public void hide() {
        this.bufferedImage = null;
    }
}
