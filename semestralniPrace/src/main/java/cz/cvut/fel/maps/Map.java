package cz.cvut.fel.maps;

import cz.cvut.fel.enemy.Goomba;
import cz.cvut.fel.gameManager.EndPoint;
import cz.cvut.fel.friend.Prize;
import cz.cvut.fel.platforms.OrdinaryBrick;
import cz.cvut.fel.platforms.Platform;
import cz.cvut.fel.platforms.SurpriseBrick;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

/**
 * class holding information of the map
 */

public class Map {
    private double remainingTime;
    private ArrayList<Platform> platformArrayList = new ArrayList<>();
    public ArrayList<Rectangle> hitboxes  = new ArrayList<>();


    private ArrayList<Goomba> goombas = new ArrayList<>();
    public ArrayList<SurpriseBrick> surpriseBricks = new ArrayList<>();
    public ArrayList<OrdinaryBrick> ordinaryBricks = new ArrayList<>();
    //private ArrayList<Fireball> fireballs = new ArrayList<>();
    public ArrayList<Prize> prizeArrayList = new ArrayList<>();


    private BufferedImage liveCounterImage;
    private BufferedImage coinCounterImage;
    public void setGoombas(ArrayList<Goomba> goombas) {
        this.goombas = goombas;
    }

    public void setPrizeArrayList(ArrayList<Prize> prizeArrayList) {
        this.prizeArrayList = prizeArrayList;
    }

    private EndPoint endPoint;
    private BufferedImage backgroundImage;
    //private double bottomBorder = 720 - 96;
    private String path;

    public void addPlatform(Platform platform){
        platformArrayList.add(platform);
    }

    public ArrayList<Rectangle> getPlatformArrayListHitboxes() {
        ArrayList<Rectangle> platformArrayListHitboxes = new ArrayList<>();
        for (Platform platform : platformArrayList) {
            platformArrayListHitboxes.add(platform.getHitbox());
        }
        return platformArrayListHitboxes;
    }

    public void addPrize(Prize platform){
        prizeArrayList.add(platform);
    }

    public void addBrick(OrdinaryBrick ordinaryBrick){ordinaryBricks.add(ordinaryBrick);}
    public void addGoomba(Goomba goomba){
        goombas.add(goomba);
    }
    public void addSurpriseBrick(SurpriseBrick platform){
        surpriseBricks.add(platform);
    }

    public void setPath(String path) {
    }

    public void setEndPoint(EndPoint endPoint) {
        this.endPoint = endPoint;
    }

    @Override
    public String toString() {
        return "Map{" +
                "goombas=" + goombas +
                ", surprise bricks=" + surpriseBricks +
                '}';
    }

    public ArrayList<Platform> getPlatformArrayList() {
        return platformArrayList;
    }

    public ArrayList<Goomba> getGoombas() {
        return goombas;
    }

    public ArrayList<Prize> getPrizeArrayList() {
        return prizeArrayList;
    }


    public EndPoint getEndPoint() {
        return endPoint;
    }


    public BufferedImage getLiveCounterImage() {
        return liveCounterImage;
    }

    public void setLiveCounterImage(BufferedImage liveCounterImage) {
        this.liveCounterImage = liveCounterImage;
    }

    public BufferedImage getCoinCounterImage() {
        return coinCounterImage;
    }

    public void setCoinCounterImage(BufferedImage coinCounterImage) {
        this.coinCounterImage = coinCounterImage;
    }

    /**
     * moves the map
     * @param scrollStep determines, how much it should move
     */

    public void scrollMap(int scrollStep) {
        for (Platform platform : this.getPlatformArrayList()) {
            int newX = platform.getX() - scrollStep;
            platform.setX(newX);

        }
        for (Goomba goomba : this.getGoombas()) {
            int newX = goomba.getX() - scrollStep;
            goomba.setX(newX);
        }

        for (Prize platform : this.getPrizeArrayList()) {
            int newX = platform.getX() - scrollStep;
            platform.setX(newX);

        }
        int endpointNewX = endPoint.getX()-scrollStep;
        this.getEndPoint().setX(endpointNewX);

    }
}