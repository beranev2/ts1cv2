
//prevzato z https://github.com/ahmetcandiroglu/Super-Mario-Bros
package cz.cvut.fel.maps;

import cz.cvut.fel.enemy.Goomba;
import cz.cvut.fel.friend.Coin;
import cz.cvut.fel.friend.FireFlower;
import cz.cvut.fel.friend.Mushroom;
import cz.cvut.fel.friend.Prize;
import cz.cvut.fel.gameManager.EndPoint;
import cz.cvut.fel.platforms.SurpriseBrick;
import cz.cvut.fel.platforms.Platform;
import cz.cvut.fel.utils.LoadSave;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.Random;

import static cz.cvut.fel.utils.Constants.GROUND_LINE;


public class MapBuilder {



        private LoadSave imageLoader;

        private BufferedImage backgroundImage;
        private BufferedImage superMushroom, oneUpMushroom, fireFlower, coin;
        private BufferedImage ordinaryBrick, surpriseBrick, groundBrick, pipe;
        private BufferedImage goombaLeft, goombaRight, koopaLeft, koopaRight, endFlag, heart;


        public MapBuilder(LoadSave imageLoader) {

            BufferedImage sprite = LoadSave.GetSpriteAtlas ("sprite.png");

            this.backgroundImage = LoadSave.GetSpriteAtlas("background.png");
            this.superMushroom = sprite.getSubimage( 48, 4*48, 48, 48);
            this.oneUpMushroom= sprite.getSubimage( 48, 4*48, 48, 48);
            this.fireFlower= sprite.getSubimage( 3*48, 4*48, 48, 48);
            this.coin = sprite.getSubimage( 0, 4*48, 48, 48);
            this.ordinaryBrick = sprite.getSubimage( 0, 0, 48, 48);
            this.surpriseBrick = sprite.getSubimage( 48, 0, 48, 48);
            this.groundBrick = sprite.getSubimage( 2, 2, 48, 48);
            this.pipe = sprite.getSubimage( 3, 1, 96, 96);
            this.goombaLeft = sprite.getSubimage( 48, 3*48, 48, 48);
            //this.goombaRight = sprite.getSubimage( 5, 4, 48, 48);
            this.koopaLeft = sprite.getSubimage( 1, 3, 48, 64);
            this.koopaRight = sprite.getSubimage( 3*48, 0, 48, 64);
            this.endFlag = sprite.getSubimage(4*48, 0, 48, 48);
            this.heart = LoadSave.GetSpriteAtlas("heart.png");

        }
//to add: time limit
public Map createMap(String mapPath) {
            BufferedImage mapImage = imageLoader.GetSpriteAtlas(mapPath);

            if (mapImage == null) {
                System.out.println("Given path is invalid...");
                return null;
            }

            Map createdMap = new Map();
            //createdMap.setBooleans(createdMap.booleans);

            createdMap.setCoinCounterImage(this.coin);
            createdMap.setLiveCounterImage(this.heart);
            int pixelMultiplier = 48;

            int mario = new Color(160, 160, 160).getRGB();
            int ordinaryBrick = new Color(0, 0, 255).getRGB();
            int surpriseBrick = new Color(255, 255, 0).getRGB();
            int groundBrick = new Color(255, 0, 0).getRGB();
            int pipe = new Color(0, 255, 0).getRGB();
            int goomba = new Color(0, 255, 255).getRGB();
            int koopa = new Color(255, 0, 255).getRGB();
            int end = new Color(160, 0, 160).getRGB();



            for (int x = 0; x < mapImage.getWidth(); x++) {
                for (int y = 0; y < mapImage.getHeight(); y++) {

                    int currentPixel = mapImage.getRGB(x, y);
                    int xLocation = x*pixelMultiplier;
                    int yLocation = y*pixelMultiplier;

                    if (currentPixel == ordinaryBrick || currentPixel == groundBrick) {
                        Platform brick = new Platform(xLocation, yLocation, 48,48,this.ordinaryBrick);
                        createdMap.addPlatform(brick);
                        //createdMap.addBrick(brick);
                        createdMap.hitboxes.add(brick.getHitbox());
                        //createdMap.booleans[xLocation][yLocation] = false;
                    }
                    else if (currentPixel == surpriseBrick ) {
                        SurpriseBrick surpriseBrick1 = new SurpriseBrick(xLocation, yLocation,48, 48,this.surpriseBrick,generatePrize(xLocation,yLocation+48));
                        createdMap.hitboxes.add(surpriseBrick1.getHitbox());
                        createdMap.addPlatform(surpriseBrick1);
                        createdMap.addSurpriseBrick(surpriseBrick1);
                        createdMap.addPrize(surpriseBrick1.prize);
                    }

                    else if (currentPixel == goomba) {
                        Goomba goomba1 = new Goomba(xLocation, yLocation, 48, 48,goombaLeft);
                        createdMap.addGoomba(goomba1);
                        createdMap.hitboxes.add(goomba1.getHitbox());
                    }
//
                    EndPoint endPoint= new EndPoint(8030, GROUND_LINE-this.endFlag.getHeight(), endFlag);
                    createdMap.setEndPoint(endPoint);


                }
            }



            System.out.println("Map is created..");
            return createdMap;
        }

    private Prize generatePrize(int x, int y) {
        Random random = new Random();
        int prize = random.nextInt(3);
        return switch (prize) {
            case 0 -> new FireFlower(x, y, 48, 48,this.fireFlower);
            case 1 -> new Coin(x, y, 48, 48, this.coin);
            case 2 -> new Mushroom(x, y, this.oneUpMushroom);
            default -> null;
        };
    }


}


