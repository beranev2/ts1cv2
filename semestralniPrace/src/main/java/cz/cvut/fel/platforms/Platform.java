package cz.cvut.fel.platforms;

import cz.cvut.fel.gameManager.GameObject;

import java.awt.*;
import java.awt.image.BufferedImage;

public class Platform extends GameObject {

    public BufferedImage bufferedImage;
    public Platform(int x, int y, int width, int height,BufferedImage bufferedImage) {
        super(x, y,width,height,bufferedImage);
        this.bufferedImage = bufferedImage;
    }
    public Rectangle getHitbox() {
        return new Rectangle(this.getX(), this.getY(), this.getWidth(), this.getHeight());
    }

}


