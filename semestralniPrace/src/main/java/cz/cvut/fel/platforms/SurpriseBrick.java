package cz.cvut.fel.platforms;


import cz.cvut.fel.friend.Prize;
import cz.cvut.fel.utils.LoadSave;

import java.awt.image.BufferedImage;


public class SurpriseBrick extends Platform {
    public Prize prize;
    private boolean isEmpty = false;
    public SurpriseBrick(int x, int y, int width, int height, BufferedImage bufferedImage, Prize prize) {
        super(x, y, width, height, bufferedImage);
        this.prize = prize;
    }
    public void revealPrize() {
        BufferedImage newStyle = LoadSave.GetSpriteAtlas("sprite.png");
        newStyle = newStyle.getSubimage( 0, 48, 48, 48);
        if(prize != null){
            prize.setRevealed(true);
        }
        setEmpty(true);
        this.bufferedImage = newStyle;
        prize.setX(this.getX());
        prize.setY(this.getY()-this.prize.getHeight());
    }
    public void setEmpty(boolean b) {
        this.isEmpty = b;
    }
    public boolean isEmpty() {
        return isEmpty;
    }
}
