package cz.cvut.fel.platforms;
import java.awt.image.BufferedImage;
public class OrdinaryBrick extends Platform {
    public BufferedImage bufferedImage;
    public OrdinaryBrick(int x, int y, BufferedImage bufferedImage) {
        super(x, y, 48, 48,bufferedImage);
    }
}
