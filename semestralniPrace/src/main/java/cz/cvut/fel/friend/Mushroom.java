package cz.cvut.fel.friend;

import java.awt.*;
import java.awt.image.BufferedImage;

public class Mushroom extends Prize {
    public Mushroom(int x, int y,BufferedImage bufferedImage) {
        super(x, y, 48, 48,bufferedImage);
    }
    public void draw(Graphics g){
        if(this.isRevealed()){
            g.drawImage(bufferedImage, this.getX(), this.getY(), null);
        }
    }
}
