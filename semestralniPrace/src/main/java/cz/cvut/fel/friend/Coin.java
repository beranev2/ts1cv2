package cz.cvut.fel.friend;

import java.awt.image.BufferedImage;
/**
 * This class implements coins, which player collects throughout the game
 */
public class Coin extends Prize {
    public Coin(int x, int y, int width, int height,BufferedImage bufferedImage) {
        super(x, y, width, height,bufferedImage);
    }

}
