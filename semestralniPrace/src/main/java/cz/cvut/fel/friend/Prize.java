package cz.cvut.fel.friend;

import cz.cvut.fel.gameManager.GameObject;
import cz.cvut.fel.gameManager.Player;
import cz.cvut.fel.gameManager.Game;

import java.awt.*;
import java.awt.image.BufferedImage;

public class Prize extends GameObject {
    int x, y;
    int height=48;
    int width=48;
    boolean empty = false;

    public boolean isEmpty() {
        return empty;
    }

    public void setEmpty(boolean empty) {
        this.empty = empty;
    }

    private boolean revealed = false;
    Rectangle rectangle = new Rectangle(x,y,width,height);
    BufferedImage bufferedImage;

    public Prize(int x, int y, int width, int height, BufferedImage bufferedImage) {
        super(x, y, width, height,bufferedImage);
        this.bufferedImage = bufferedImage;
    }

    int getPoint() {
        return 0;
    }

    public boolean isRevealed() {
        return revealed;
    }

    public void setRevealed(boolean revealed) {
        this.revealed = revealed;
    }

    public void draw(Graphics g){
        if(this.isRevealed()){
            g.drawImage(bufferedImage, (int)getX(), (int)getY(), null);
        }
    }

    Rectangle getBounds() {
        return null;
    }

    void onTouch(Player player, Game game) {

    }


    @Override
    public int getX() {
        return x;
    }

    @Override
    public void setX(int x) {
        this.x = x;
    }

    @Override
    public int getY() {
        return y;
    }

    @Override
    public void setY(int y) {
        this.y = y;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public Rectangle getRectangle() {
        return rectangle;
    }

    public void setRectangle(Rectangle rectangle) {
        this.rectangle = rectangle;
    }

    public BufferedImage getBufferedImage() {
        return bufferedImage;
    }

    public void setBufferedImage(BufferedImage bufferedImage) {
        this.bufferedImage = bufferedImage;
    }

    public Rectangle getHitbox() {
        return new Rectangle(x, y, width, height);
    }

    public void hide() {
        setEmpty(true);
        this.bufferedImage = null;
    }
}
