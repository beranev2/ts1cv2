package cz.cvut.fel.friend;

import cz.cvut.fel.gameManager.GameObject;
import cz.cvut.fel.enemy.Goomba;

import java.awt.image.BufferedImage;
import java.util.ArrayList;

public class Fireball extends GameObject {
    int FIREBALL_SPEED = 100;
    BufferedImage image;
    boolean isEmpty = false;
    public Fireball(int x, int y, BufferedImage bufferedImage) {
        super(x, y, 24, 24, bufferedImage);
        this.image = bufferedImage;
    }
    /**
    this method moves the fireball
     */
    public void updateFireball(){
        this.setX(this.getX()+FIREBALL_SPEED);
    }
    public BufferedImage getImage() {
        return image;
    }
    /**
     this method checks for collisions with goombas
     */
    public boolean killGoomba (ArrayList<Goomba> goombas){
        for(Goomba goomba:goombas){
            if (this.getHitbox().intersects(goomba.getHitbox())){
                goomba.hide();
                goombas.remove(goomba);
                return true;
            }
        }
        return false;
    }
    public void hide() {
        this.image = null;
    }
    public boolean isEmpty() {
        return isEmpty;
    }
    public void setEmpty(boolean empty) {
        isEmpty = empty;
    }
}
