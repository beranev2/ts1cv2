package cz.cvut.fel.friend;

import java.awt.image.BufferedImage;

public class FireFlower extends Prize {
    BufferedImage bufferedImage;
    public FireFlower(int x, int y, int width, int height, BufferedImage img) {
        super(x, y, width, height, img);
        this.bufferedImage = img;
    }

}
